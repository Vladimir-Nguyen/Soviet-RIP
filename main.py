# import libraries
import re
import os, time

# function list all branch
def listAllBranch(location):
    os.system("cd " + location + "/ && git branch -a | grep remotes/origin/ > all_branch.txt")

    with open(location +"/all_branch.txt") as file:
        print "\nAll branch in the project: "
        for j, b in enumerate(file):
            if(j > 0):
                b = re.sub(" remotes\/origin\/", "", str(b))
                print "\t[+] " + str(b.strip())

    with open(location + "/all_branch.txt") as file:
        for i, line in enumerate(file):
            if (i == 0):
                continue
            else:
                line = re.sub(" remotes\/origin\/", "", str(line))
                print "\n========== Program is scanning with branch [" + str(line.strip()) + "] ==============\n"
                os.system("git checkout " + str(line))
                os.system("sniffgit --root " + location)

# print banner
os.system("cat banner.txt")

# function fetch git
link_repo = raw_input("\nPlease input link repo: ")
os.system("cd /tmp/repo_test/ && git clone " + link_repo)
#os.system("git clone " + link_repo)
time.sleep(7)
name_dir = re.sub(".*\/","",link_repo)
name_dir = re.sub("\.git","",name_dir)
name_dir = "/tmp/repo_test/" + name_dir
print "Scanning directory: " + name_dir

# check sensitive information in branch
listAllBranch(name_dir)

print "Done"